# Inoffizielle AoD App

## Features
* Zugriff auf deine komplette AoD Bibliothek
* Durchsuche die AoD Bibliothek nach deinen Favoriten
* Sieh was neu ist auf AoD
* entscheide dich ob du die Serie mit Dub oder OmU sehen möchtest (sofern vorhanden)
* Setze die Serie dort fort wo du sie aufgehört hast

## License

This App is licensed under the terms and conditions of the AGPL license. This Project is not associated with Anime-on-Demand in any way.

## Special Thanks
Tami
