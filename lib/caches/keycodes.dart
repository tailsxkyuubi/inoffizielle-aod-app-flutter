const int KEY_UP                  = 19;
const int KEY_DOWN                = 20;
const int KEY_LEFT                = 21;
const int KEY_RIGHT               = 22;
const int KEY_CENTER              = 23;
const int KEY_MENU                = 82;
const int KEY_MEDIA_PLAY_PAUSE    = 85;
const int KEY_MEDIA_SKIP_FORWARD  = 272;
const int KEY_MEDIA_STEP_FORWARD  = 90;
const int KEY_MEDIA_STEP_BACKWARD = 89;